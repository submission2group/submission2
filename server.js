const express = require("express");
const app = express();
const bodyparser = require("body-parser");
const mysql = require("mysql");

app.use(bodyparser.urlencoded({ extended: "true" }))
app.use(bodyparser.json());

const conn = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "manager",
    database: "node"
})
conn.connect(function (err) {
    if (err) console.log(err)
})
app.get("/", (req, res) => {

    let sql = "select * from movies";
    conn.query(sql, (err, result) => {
        res.send(result);
    })
})
app.post("/", (req, res) => {
    var id = req.body.id
    var title = req.body.title
    var date = req.body.date
    var time = req.body.time
    var director = req.body.director;

    let sql = `insert into movies values(${id},'${title}','${date}','${time}','${director}');`;
    conn.query(sql, (err, result) => {
        if (err) res.send(err)
        res.send(result);
    })
})
app.delete("/:id", (req, res) => {

    let sql = `delete from movies where id =${req.params.id};`;
    conn.query(sql, (err, result) => {
        if (err) res.send(err)
        res.send(result);
    })
})

app.listen("3000", (req, res) => {
    console.log(
        "serving on port 3000 "
    )
})